  
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, NavLink, Redirect, Prompt} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Login from './auth/Login';
import RyMComponent from './RyMComponent';
import TitleComponent from './TitleComponent';
import axios from "axios";

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: "",
      password: "",
      loginErrors: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { user, password } = this.state;

    axios
      .post(
        "http://localhost:3001/auth",
        {
          user: {
            user: user,
            password: password
          }
        },
      )
      .then(response => {
        if (response["data"]["msg"] == "authorized") {
          console.log(response["data"]["msg"]);
        }else{
          alert("Failed Login. user: watermelon password: rulez")
        }
      })
      .catch(error => {
        console.log("login error", error);
      });
    event.preventDefault();
  }

  render() {
    return (
      <Router>
         <Route exact path='/' render={() => {
          return(<div class="main">
          <div class="col-md-6 col-sm-12">
              <div class="login-form">
                <form onSubmit={this.handleSubmit}>
                  <div className="input-group" class="form-group">
                    <label>User Name</label>
                    <input
                        type="text"
                        name="user"
                        placeholder="user"
                        value="watermelon"
                        onChange={this.handleChange}
                        required
                      />
                  </div>
                  <div class="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value="rulez"
                        onChange={this.handleChange}
                        required
                      />
                  </div>
                </form>
              </div>
            </div>
        </div>)
        }
      }
        />   
        <div className="main">
        <Link to="/dash" align="center" type="button" className="login-btn" class="btn btn-black">login</Link>
        </div>
        <Route path="/dash">
        <TitleComponent />              
        <RyMComponent />        
        </Route>             
      </Router>
    );
  }
}

export default App;