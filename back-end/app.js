var express = require('express'),
    app = express(),
    http = require('http'),
    path = require('path'),
    router = express.Router(),
    bodyParser  = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session');

var cors = require('cors');
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }
app.use(cors());

app.use(bodyParser.json());
app.use(methodOverride());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: 'watermelon', cookie: { maxAge: 120000 }}));

app.use(router);

// GET method route
router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/views/login.html'));
 });

 var Request = require("request");

router.get('/rickyandmorty', cors(), function (req, res, next) {
    Request.get("https://rickandmortyapi.com/api/character", (error, response, body) => {
        if(error) {
            return res.json({"message": "Error"});
        }
        return res.json(JSON.parse(body));
    });
 }) 
 // POST method route
 router.post('/auth', cors(corsOptions), function (req, res) {
     var login = req.body;
      console.log(login);

     if(login['user']['user']=='watermelon' && login['user']['password']=='rulez'){
         res.json({msg: 'authorized'});
     }        
     else{
        res.json({msg: 'failed'});
     }       
   });

 // POST method route
 router.post('/sessions', cors(corsOptions), function (req, res) {
    var login = req.body;
     console.log(login);

    if(login['user']['email']=='watermelon@gmail.com' && login['user']['password']=='rulez'){
        res.json({msg: 'authorized'});
    }        
    else{
       res.json({msg: 'failed'});
    }       
  });


app.listen(3001, function() {
console.log('Node server running on http://localhost:3001');
});