import React, { Component } from 'react';

class LoginComponent extends Component {

  constructor(props){
    super(props);
    this.state = {};
  }
  onSubmit () {
    const value = {
      username : this.state.username,
      password : this.state.password
    };
    console.log(value);
  }
    render() {
      return (
        <div class="main">
          <div class="col-md-6 col-sm-12">
              <div class="login-form">
                <form>
                  <div className="input-group" class="form-group">
                    <label htmlFor="username">User Name</label>
                    <input type="text"  className="login-input" value={this.state.username} class="form-control" placeholder="User Name"></input>
                  </div>
                  <div className="input-group" class="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="login-input" value={this.state.password} class="form-control" placeholder="Password"></input>
                  </div>
                  <button type="button" className="login-btn" onClick={() => this.onSubmit()}  class="btn btn-black">Login</button>
                </form>
              </div>
            </div>
        </div>
      )
    }
}

export default LoginComponent;