import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import $ from 'jquery';

class RyMComponent extends Component {
    constructor(props) {
      super(props);
  
      this.state = {character: []};
    }
  
    componentDidMount() {
      this.CharacterList();
    }
  
    CharacterList() {
      $.getJSON('http://localhost:3001/rickyandmorty/')
        .then(({ results }) => this.setState({ character: results }));
    }
  
    render() {
      const characters = this.state.character.map((item, i) => (       
          <div class="row">
            <div class="col">          
              <table class="table table-responsive table-hover">
                <tbody>
                    <tr>
                      <td>
                        <tr>
                          <td><img src={ item.image } /></td>
                        </tr>
                      </td>
                      <td>
                        <tr><td>{ item.name }</td></tr>
                        <tr><td>{ item.status }</td></tr>
                        <tr><td>{ item.species }</td></tr>
                        <tr><td>{ item.gender }</td></tr>
                        <tr><td>{ item.location.name }</td></tr>
                        <tr><td><a href={ item.location.url } target="_blank" type="button">Enlace</a></td></tr>
                      </td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
      ));
  
      return (<div className="character-list">{ characters }</div>);
    }
  }
  
  export default RyMComponent;