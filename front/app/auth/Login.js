import React, { Component } from "react";
import { Redirect, Router, Route } from 'react-router';
import axios from "axios";
import RyMComponent from '../RyMComponent';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: "",
      password: "",
      loginErrors: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { user, password } = this.state;

    axios
      .post(
        "http://localhost:3001/auth",
        {
          user: {
            user: user,
            password: password
          }
        },
      )
      .then(response => {
        if (response["data"]["msg"] == "authorized") {
          console.log(response["data"]["msg"]);
        }else{
          alert("Failed Login. user: watermelon password: rulez")
        }
      })
      .catch(error => {
        console.log("login error", error);
      });
    event.preventDefault();
  }

  render() {
    return (
      <Router>
        <Route exact path='/' render={() => {
          return(<div class="main">
          <div class="col-md-6 col-sm-12">
              <div class="login-form">
                <form onSubmit={this.handleSubmit}>
                  <div className="input-group" class="form-group">
                    <label>User Name</label>
                    <input
                        type="text"
                        name="user"
                        placeholder="user"
                        value={this.state.user}
                        onChange={this.handleChange}
                        required
                      />
                  </div>
                  <div class="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        required
                      />
                  </div>
                  <Link to="/dash">lOGIN</Link>
                  <Route path="/dash">
                    <RyMComponent />
                  </Route>
                  
                </form>
              </div>
            </div>
        </div>)
        }
      }
        />        
      </Router>
    );
  }
}